#pragma once
class Vector
{
public:

	float x;
	float y;
	float z;

	float norm();
	void normalize();
	Vector cross(Vector v);
	void equal(float k);


public:
	Vector() : x(0), y(0), z(0) {}

	Vector(const float x, const float y, const float z)
		:x(x), y(y), z(z){}

	Vector operator+(const Vector v) const
	{
		return Vector(x + v.x, y + v.y, z + v.z);
	}
	Vector operator+=(const Vector v) const
	{
		return Vector(x + v.x, y + v.y, z + v.z);
	}


	Vector operator-(const Vector v) const
	{
		return Vector(x - v.x, y - v.y, z - v.z);
	}
	Vector operator-=(const Vector v) const
	{
		return Vector(x - v.x, y - v.y, z - v.z);
	}

	Vector operator*(const Vector v) const
	{
		return Vector(x*v.x, y*v.y, z*v.z);
	}
	Vector operator*(float k) const
	{
		return Vector(k*x, k*y, k*z);
	}
	friend Vector operator*(float k, Vector v)
	{
		return Vector(k*v.x, k*v.y, k*v.z);
	}

	Vector operator/(float k) const
	{
		return Vector(x / k, y / k, z / k);
	}
};

