#pragma once
#include <vector>
#include "Vector.h"
#include "Particle.h"
#include "Grid.h"

class SPH
{

public:
	int np;		//number of paricles
	int d0;		//rest density
	float q;	//distance between two particles
	float h;	//영향권 반경
	float k;	//이상기체상수
	float mu;	//물의 점성계수
	float s;	//tension coefficient
	float pi = 3.14;
	float t = 0.03;
	
	std::vector<Particle> P;
	Grid hash[100][100];

public:
	SPH();
	~SPH();

	float Poly6(Vector r, float h);
	float L_Poly6(Vector r, float h);
	Vector Spiky(Vector r, float h);
	float Viscosity(Vector r, float h);
	void Initialize();
	void Update_data();
	void Render();

	void Density();
	void Pressure();
	void Pressure_Force();
	void Viscosity_Force();
	void Surface_Tesion_Force();
	void Externel_Force();


	// for test
	void MeanVelo();
	void dist_com();
};