#pragma once
#include "Vector.h"

class Particle
{
public:
	Particle();
	~Particle();

	Vector r;	//Position
	Vector a;	//Accel
	Vector v;	//Velocity
	float d;	//밀도
	float p;	//압력
	float m;	//질량

	Vector Fe;	//external froce
	Vector Fp;	//pressure Force
	Vector Fv;	//viscosity Force
	Vector Fs;	//surface Tension Force
	Vector F;	//total Force

	Vector com;
};

