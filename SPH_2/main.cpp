#include <stdio.h>
#include <vector>
#include <stdlib.h>
#include <gl\GL.h>
#include <gl\GLU.h>
#include <gl\glaux.h>
#include <gl\glut.h>
#include "SPH.h"

int mode = -1;	//Ű����

SPH S;

void Initialize()
{
	S.Initialize();
}

void Render()
{
	S.Render();

	glutSwapBuffers();
}

void Idle()
{
	if (mode == 1)
	{
		S.Update_data();
	}

	glutPostRedisplay();
}

void Keyboard(unsigned char key, int x, int y)
{
	switch (key)
	{
	case 32:
		mode = mode * -1;
		break;
	case 'q':
		exit(0);
		break;
	case 27:
		exit(0);
		break;
	default:
		break;
	}
	glutPostRedisplay();
}

void Reshape(int w, int h)
{
	glViewport(0, 0, w, h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(60.0, w / h, 1.0, 100.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

int main(int argc, char* argv[])
{
	Initialize();
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGBA | GLUT_DEPTH | GLUT_DOUBLE);
	glutInitWindowSize(800, 800);
	glutCreateWindow("Smoothed Particle Hydrodynamics");
	glutDisplayFunc(Render);
	glutReshapeFunc(Reshape);
	glutKeyboardFunc(Keyboard);
	glutIdleFunc(Idle);

	glutMainLoop();

	return 0;
}