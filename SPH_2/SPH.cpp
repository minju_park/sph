#include "SPH.h"
#include "Vector.h"
#include "Particle.h"
#include "Grid.h"
#include "FileManager.h"
#include <vector>
#include <gl\GL.h>
#include <gl\GLU.h>
#include <gl\glaux.h>
#include <gl\glut.h>

SPH::SPH()
{
	np = 0;		//입자 갯수
	d0 = 4.0;	//기준밀도
	q = 0.5;	//입자 사이의 거리
	h = 1.0;	//영향권 반경
	k = 45.3;	//이상기체상수
	mu = 1.8;	//물의 점성계수
	s = 10.0;	//tension coefficient //20.f
	pi = 3.14;
	t = 0.03;
}

FileManager cap;

SPH::~SPH()
{
}

float SPH::Poly6(Vector r, float h)
{
	float W, nr = sqrt(r.x*r.x + r.y*r.y + r.z*r.z);

	if (nr >= 0.0 && nr <= h)
		W = (315 * (h*h - nr*nr)*(h*h - nr*nr)*(h*h - nr*nr)) / (64 * pi*pow(h, 9));
	else
		W = 0.0;

	return W;
}

float SPH::L_Poly6(Vector r, float h)
{
	float W, nr = sqrt(r.x*r.x + r.y*r.y + r.z*r.z);

	if (nr >= 0.0 && nr <= h)
		W = (-945 * (5 * nr*nr - h*h)*(nr*nr - h*h)) / (32 * pi*pow(h, 9));
	else
		W = 0.0;

	return W;
}

Vector SPH::Spiky(Vector r, float h)
{
	Vector W;
	float nr = r.norm();

	if (nr >= 0.0 && nr <= h)
	{
		r.normalize();
		W = r * (-45.0f * (h - nr)*(h - nr) / (pi*pow(h, 6)));
	}
	else
		W.equal(0.0f);

	return W;
}

float SPH::Viscosity(Vector r, float h)
{
	float W, nr = sqrt(r.x*r.x + r.y*r.y + r.z*r.z);

	if (nr >= 0.0 && nr <= h)
		W = (45 * (h - nr)) / (pi*pow(h, 6));
	else
		W = 0;

	return W;
}

void SPH::Initialize()
{
	for (float i = 1.1; i < 60.0; i += 0.5)
	{
		for (float j = 1.1; j < 60.0; j += 0.5)
		{
			Particle p0;
			p0.r.x = i;
			p0.r.y = j;
			p0.r.z = 0.0;

			p0.v.equal(0.0);
			p0.a.equal(0.0);
			p0.m = 1.0;
			p0.d = d0;
			p0.p = 0.0;
			p0.Fv.equal(0.0);
			p0.Fp.equal(0.0);
			p0.F.equal(0.0);

			P.push_back(p0);
			np++;
		}
	}

	//Hash에 particle 정보 저장
	for (int i = 0; i < np; i++)
	{
		int hx = P[i].r.x;
		int hy = P[i].r.y;

		hash[hx][hy].push_Particle(&P[i]);
	}
}

void SPH::Density()
{
	for (int i = 0; i < np; i++)
	{
		int hx = P[i].r.x;
		int hy = P[i].r.y;

		float dens = 0.0;

		for (int j = -1; j <= 1; j++)
		{
			for (int k = -1; k <= 1; k++)
			{
				for (int l = 0; l < hash[hx + j][hy + k].pP.size(); l++)
				{
					Vector o = P[i].r - hash[hx + j][hy + k].pP[l]->r;

					dens += (hash[hx + j][hy + k].pP[l]->m) * Poly6(o, h);
				}
			}
		}
		P[i].d = dens;
	}
}

void SPH::Pressure()
{
	for (int i = 0; i < np; i++)
	{
		P[i].p = k*(P[i].d - d0);
	}
}

void SPH::Pressure_Force()
{
	for (int i = 0; i < np; i++)
	{
		int hx = P[i].r.x;
		int hy = P[i].r.y;

		Vector pf;
		pf.equal(0.0f);

		for (int j = -1; j <= 1; j++)
		{
			for (int k = -1; k <= 1; k++)
			{
				for (int l = 0; l < hash[hx + j][hy + k].pP.size(); l++)
				{
					Vector o = P[i].r - hash[hx + j][hy + k].pP[l]->r;
					float no = o.norm();

					if (no > 0.0)
					{
						pf = pf + (Spiky(o, h)*(-P[l].m*(P[i].p + hash[hx + j][hy + k].pP[l]->p) / (2 * hash[hx + j][hy + k].pP[l]->d)));
					}
				}
			}
		}
		P[i].Fp = pf;
	}
}

void SPH::Viscosity_Force()
{
	for (int i = 0; i < np; i++)
	{
		int hx = P[i].r.x;
		int hy = P[i].r.y;

		Vector vf;
		vf.equal(0.0);

		for (int j = -1; j <= 1; j++)
		{
			for (int k = -1; k <= 1; k++)
			{
				for (int l = 0; l < hash[hx + j][hy + k].pP.size(); l++)
				{
					Vector o = P[i].r - hash[hx + j][hy + k].pP[l]->r;
					Vector u = hash[hx + j][hy + k].pP[l]->v - P[i].v;

					vf = vf + mu * hash[hx + j][hy + k].pP[l]->m * u * Viscosity(o, h) / hash[hx + j][hy + k].pP[l]->d;
				}
			}
		}
		P[i].Fv = vf;
	}
}

void SPH::Surface_Tesion_Force()
{
	for (int i = 0; i < np; i++)
	{
		int hx = P[i].r.x;
		int hy = P[i].r.y;

		Vector sf;
		sf.equal(0.0);

		for (int j = -1; j <= 1; j++)
		{
			for (int k = -1; k <= 1; k++)
			{
				for (int l = 0; l < hash[hx + j][hy + k].pP.size(); l++)
				{
					Vector o = P[i].r - hash[hx + j][hy + k].pP[l]->r;
					float no = o.norm();

					if (no > 0.0)
					{
						sf = sf + -s * hash[hx + j][hy + k].pP[l]->m * L_Poly6(o, h) * o / (no*hash[hx + j][hy + k].pP[l]->d);
					}
				}
			}
		}
		P[i].Fs = sf;
	}
}

void SPH::Externel_Force()
{
	for (int i = 0; i < np; i++)
	{
		P[i].Fe.x = 0.0;
		P[i].Fe.y = -9.8*P[i].m;
		P[i].Fe.z = 0.0;
	}
}

void SPH::Update_data()
{
	Density();
	Pressure();
	Pressure_Force();
	Viscosity_Force();
	Surface_Tesion_Force();
	Externel_Force();

	MeanVelo();
	dist_com();

	for (int i = 0; i < np; i++)
	{
		//**********Total Force
		P[i].F = P[i].Fp + P[i].Fv + P[i].Fs + P[i].Fe;

		//**********New Acceleration
		P[i].a = P[i].F / P[i].d;

		//**********New Velocity
		P[i].v = P[i].v + P[i].a*t;

		//**********New Position
		P[i].r = P[i].r + P[i].v*t;

		if (P[i].r.y < 1.0)
		{
			P[i].v.y = -P[i].v.y * 0.1f;
			P[i].r.y = 1.01;
		}

		if (P[i].r.y >= 99.0)
		{
			P[i].v.y = -P[i].v.y * 0.1f;
			P[i].r.y = 98.99;
		}

		if (P[i].r.x < 1.0)
		{
			P[i].v.x = -P[i].v.x * 0.1f;
			P[i].r.x = 1.01;
		}

		if (P[i].r.x >= 99.0)
		{
			P[i].v.x = -P[i].v.x * 0.1f;
			P[i].r.x = 98.99;
		}
	}

	//**********CLEAR HASH
	for (int i = 0; i < 100; i++)
	{
		for (int j = 0; j < 100; j++)
		{
			hash[i][j].clear_Particle();
		}
	}

	//**********DRAW HASH
	for (int i = 0; i < np; i++)
	{
		int hx = P[i].r.x;
		int hy = P[i].r.y;

		hash[hx][hy].push_Particle(&P[i]);
	}
}

void SPH::Render()
{
	glEnable(GL_DEPTH_TEST);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();

	gluLookAt(50.0, 50.0, 86.7, 50.0, 50.0, 0.0, 0.0, 1.0, 0.0);

	glLineWidth(1.0);
	glPointSize(2);

	glColor3f(1.0f, 1.0f, 1.0f);
	glBegin(GL_LINE_LOOP);
	glVertex3f(1.0, 1.0, 0.0);
	glVertex3f(99.0, 1.0, 0.0);
	glVertex3f(99.0, 99.0, 0.0);
	glVertex3f(1.0, 99.0, 0.0);
	glEnd();

	glBegin(GL_POINTS);
	for (int i = 0; i < np; i++)
	{
		if (P[i].Fs.norm()>20.0f&&P[i].v.norm()>3.0f)
			glColor3f(1.0f, 1.0f, 1.0f);
		else
			glColor3f(1.0f, 0.0f, 0.0f);

		glVertex3f(P[i].r.x, P[i].r.y, P[i].r.z);
	}
	glEnd();
	
	//cap.ScreenCapture();
}

void SPH::MeanVelo()
{
	Vector sum;	sum.equal(0.0f);

	for (int i = 0; i < np; i++)
	{
		sum.x += P[i].v.x;
		sum.y += P[i].v.y;
		sum.z += P[i].v.z;
	}

	sum.x /= np;
	sum.y /= np;
	sum.z /= np;

	float m = sum.norm();
	
}

void SPH::dist_com()
{
	for (int i = 0; i < np; i++)
	{
		if (P[i].Fs.norm() > 20.0f)
		{
			Vector distance; distance.equal(0.0f);
			Vector sum_neigbor; sum_neigbor.equal(0.0f);
			float sum_mass; sum_mass = 0.0f;

			int hx = P[i].r.x;
			int hy = P[i].r.y;
			for (int j = -1; j <= 1; j++)
			{
				for (int k = -1; k <= 1; k++)
				{
					for (int l = 0; l < hash[hx + j][hy + k].pP.size(); l++)
					{
						sum_mass += 1;
						sum_neigbor = sum_neigbor + hash[hx + j][hy + k].pP[l]->r;
					}
				}
			}

			distance = P[i].r - (sum_neigbor / sum_mass);


			P[i].com = distance;
		}
		else
			P[i].com = { 0.0f, 0.0f, 0.0f };
	}
}